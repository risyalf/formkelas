<?php

namespace App\Http\Controllers;

use App\Data;
use Illuminate\Http\Request;
use Carbon\Carbon;

class FormController extends Controller
{

    public function confirm(Request $request) {
        // save answer to database

        // 1. cek nama dan kelas apakah sudah ada belum
        // 2. simpan data-data
        // 3. arahkan ke sukses page

        $request->validate([
            'nama'      => 'required',
            'kelas'     => 'required',
            'alamat'    => 'required',
            'hobi'    => 'required',
            'materi'    => 'required'
        ]);

        
        $date = Carbon::now();

        // 2021-01-28 19:21:49
        // if($date < ('2021-01-28 19:00:00'))
        //     return "YEASSSS";
        // else
        //     return "NOOOOOOOO";

        // if ($date > ('2021-01-28 19:00:00') && $date < ('2021-01-29 19:00:00'))
        if ($date > ('2021-01-29 12:00:00') && $date < ('2021-01-29 19:00:00'))
            return back()->withErrors('Form Akan Dibuka Hari Jumat Pukul 7 Malam.');
        
        $count = Data::where('materi', $request->materi)->count();

        if($count >= 10) {
            return back()->withErrors($request->materi.' sudah dipilih oleh 10 orang siswa. Tolong pilih materi yang lainnya')->withInput();
        }

        $exist =  Data::where('nama', '=', $request->nama)
            ->where('kelas', '=', $request->kelas)
            ->first();

        if($exist == null) 
            $data = new Data;
        else 
            $data = $exist;
        
        $data->nama = $request->nama;
        $data->kelas = $request->kelas;
        $data->alamat = $request->alamat;
        $data->hobi = $request->hobi;
        $data->materi = $request->materi;
        $data->save();

        return view('success');
    }
}
