<?php

namespace App\Http\Controllers;

use App\Data;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function home() {
        // $datas = Data::all();
        $datas = Data::select("*")->paginate(10);
        return view('admin', compact('datas'));
    }
}
