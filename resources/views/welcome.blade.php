<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MATERI UNJUK KERJA DAN KARYA (UKK)</title>

    
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" 
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" 
                integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
                integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
                

    <style>
        body {
            padding: 50px 100px;
        }
        input[type="radio"] {
            vertical-align: middle;
        }
    </style>
</head>
<body>
    <div class="text-center">
        <h2>MATERI UNJUK KERJA DAN KARYA (UKK)</h2>
        <h2>SDN KEBONSARI I SURABAYA</h2>
        <h2>TAHUN 2020 -  2021</h2>
    </div>
    @if($errors->any())
        <div class="alert alert-danger" role="alert" style="margin-top: 40px">
            <h4>{{$errors->first()}}</h4>
        </div>
    @endif
    <form action="{{ route('confirm') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="nama-form">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama-form" value="{{ old('nama') }}" required>
        </div>
        <label for="kelas-form">Kelas</label>
        <br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="kelas" id="6A" value="6A" checked>
            <label class="form-check-label" for="6A">
              6A
            </label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="kelas" id="6B" value="6B">
            <label class="form-check-label" for="6B">
              6B
            </label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="kelas" id="6C" value="6C">
            <label class="form-check-label" for="6C">
              6C
            </label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="kelas" id="6D" value="6D">
            <label class="form-check-label" for="6D">
              6D
            </label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="kelas" id="6E" value="6E">
            <label class="form-check-label" for="6E">
              6E
            </label>
        </div>
        <div class="form-group" style="margin-top: 15px" >
            <label for="alamat-form">Alamat</label>
            <input type="text" class="form-control" name="alamat" id="alamat-form" value="{{ old('alamat') }}" required>
        </div>
        <div class="form-group">
            <label for="hobi-form">Hobi / Minat</label>
            <input type="text" class="form-control" name="hobi" id="hobi-form" value="{{ old('hobi') }}" required>
        </div>

        <label style="width: 100%; margin: 0">Pilih salah satu</label>
        <small for="" style="font-weight: bold;">Peringatan : Setiap materi hanya bisa dipilih 10 siswa. Jadi bila anda tidak bisa memilih suatu materi, artinya materi itu sudah dipilih oleh 10 siswa. Jika begitu silahkan pilih materi lain</small>
        <div style="margin-top: 20px">
            <label for="" style="font-size: 18px; font-weight: bold">TEMA 1 Melestarikan Lingkungan Alam (Bersama)</label>
            <br>
            <label for="">Materi :</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema1_materi1" value="tema1_materi1" checked>
                <label class="form-check-label" for="tema1_materi1">
                    Ipal (Membuat Instalasi Pengolahan Air Limbah)
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema1_materi2" value="tema1_materi2">
                <label class="form-check-label" for="tema1_materi2">
                    Pemanfaatan Minyak Jelantah (Membuat Kreasi Dari Minyak Jelantah)
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema1_materi3" value="tema1_materi3">
                <label class="form-check-label" for="tema1_materi3">
                    Pengolahan Limbah Rumah Tangga (Membuat Kompos dengan media takakura)
                </label>
            </div>
        </div>
        <div style="margin-top: 20px">
            <label for="" style="font-size: 18px; font-weight: bold">TEMA 2 Aku Dan Masyarakat  Sekitar</label>
            <br>
            <label for="">Materi :</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema2_materi1" value="tema2_materi1">
                <label class="form-check-label" for="tema2_materi1">
                    Fotografi (Membuat Fotografi Tentang Kegiatan Di Lingkungan Tempat Tinggal contoh : Kerjabakti, Peduli terhadap Isu Covid 19))                </label>
            </div>
        </div>
        <div style="margin-top: 20px">
            <label for="" style="font-size: 18px; font-weight: bold">TEMA 3 Hak dan Kewajiban</label>
            <br>
            <label for="">Materi :</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema3_materi1" value="tema3_materi1">
                <label class="form-check-label" for="tema3_materi1">
                    Videografi (Membuat Video Tentang Hak Dan Kewajiban Siswa Dilingkungan Sekolah, Rumah Dan Masyarakat)
            </div>
        </div>
        <div style="margin-top: 20px">
            <label for="" style="font-size: 18px; font-weight: bold">TEMA 4 Keadilan Sosial</label>
            <br>
            <label for="">Materi :</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema4_materi1" value="tema4_materi1">
                <label class="form-check-label" for="tema4_materi1">
                    Antalogi Puisi (Membuat Puisi Dengan Tema “Keadilan Sosial”)           
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema4_materi2" value="tema4_materi2">
                <label class="form-check-label" for="tema4_materi2">
                    Bermain Bersama (Membuat Permainan Tradisional)            
            </div>
        </div>
        <div style="margin-top: 20px">
            <label for="" style="font-size: 18px; font-weight: bold">TEMA 5 Wawasan Sosial Dan Budaya</label>
            <br>
            <label for="">Materi :</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema5_materi1" value="tema5_materi1">
                <label class="form-check-label" for="tema5_materi1">
                    Pengembangan Makanan Tradisional (Membuat/Menciptakan Menu Makanan Tradisional Baru)            
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema5_materi2" value="tema5_materi2">
                <label class="form-check-label" for="tema5_materi2">
                    Pengembangan Tarian Tradisional (Membuat Tari Kreasi Baru Lengkap Dengan Aksesorisnya)            
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema5_materi3" value="tema5_materi3">
                <label class="form-check-label" for="tema5_materi3">
                    Pengembangan Lagu Daerah (Membuat Lagu Daerah dengan Tema Covid)            
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema5_materi4" value="tema5_materi4">
                <label class="form-check-label" for="tema5_materi4">
                    Pengembangan Pembelajaran Membatik (Membuat Karya Sederhana  Dengan Hiasan Batik Ciri Khas SDN Kebonsari I)            
            </div>
        </div>
        <div style="margin-top: 20px">
            <label for="" style="font-size: 18px; font-weight: bold">TEMA 6 Nasionalisme Dan Kebangsaan</label>
            <br>
            <label for="">Materi :</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema6_materi1" value="tema6_materi1">
                <label class="form-check-label" for="tema6_materi1">
                    Cinematografi (Membuat Iklan Layanan Masyarakat Tentang Nasionalisme dengan memanfaatkan media tiktok/viva video/Kine master/video slide show,dll)
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema6_materi2" value="tema6_materi2">
                <label class="form-check-label" for="tema6_materi2">
                    Membuat Cerpen
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema6_materi3" value="tema6_materi3">
                <label class="form-check-label" for="tema6_materi3">
                    Membuat Komik
            </div>
        </div>
        <div style="margin-top: 20px">
            <label for="" style="font-size: 18px; font-weight: bold">TEMA 7 Kesejahteraan Masyarakat (Pengentasan Kemiskinan)</label>
            <br>
            <label for="">Materi :</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema7_materi1" value="tema7_materi1">
                <label class="form-check-label" for="tema7_materi1">
                    Kreasi dari kain perca
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema7_materi2" value="tema7_materi2">
                <label class="form-check-label" for="tema7_materi2">
                    Membuat makanan dan minuman inovatif
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema7_materi3" value="tema7_materi3">
                <label class="form-check-label" for="tema7_materi3">
                    Fashion
            </div>
        </div>
        <div style="margin-top: 20px">
            <label for="" style="font-size: 18px; font-weight: bold">TEMA 8 Penggunaan Teknologi yang positif dan produktif</label>
            <br>
            {{-- <label for="">Materi :</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema8_materi1" value="tema8_materi1">
                <label class="form-check-label" for="tema8_materi1">
                    ???
            </div> --}}
        </div>
        <div style="margin-top: 20px">
            <label for="" style="font-size: 18px; font-weight: bold">TEMA 9 Hidup Sehat Dan Seimbang</label>
            <br>
            <label for="">Materi :</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema9_materi1" value="tema9_materi1">
                <label class="form-check-label" for="tema9_materi1">
                    Pengembangan Senam Kreasi 
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema9_materi2" value="tema9_materi2">
                <label class="form-check-label" for="tema9_materi2">
                    Pengembangan Makanan Sehat Dan Bergizi (membuat es krim)
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema9_materi3" value="tema9_materi3">
                <label class="form-check-label" for="tema9_materi3">
                    Pengembangan Minuman Tradisional (Membuat Kreasi Minuman Tradisional Dengan Kemasan Modern) 
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema9_materi4" value="tema9_materi4">
                <label class="form-check-label" for="tema9_materi4">
                    Videografi (Membuat Video Tentang Protokol Kesehatan) 
            </div>
        </div>
        <div style="margin-top: 20px">
            <label for="" style="font-size: 18px; font-weight: bold">TEMA 10 Toleransi Dan Keberagaman</label>
            <br>
            <label for="">Materi :</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema10_materi1" value="tema10_materi1">
                <label class="form-check-label" for="tema10_materi1">
                    Filmatografi (Membuat Sketsa Tema “Toleransi Dan Keberagaman”)
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema10_materi2" value="tema10_materi2">
                <label class="form-check-label" for="tema10_materi2">
                    Menulis (Membuat Komik Kisah Ashabul Khafi dengan Tema “Toleransi”)
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="materi" id="tema10_materi3" value="tema10_materi3">
                <label class="form-check-label" for="tema10_materi3">
                    Fotografi (Membuat Foto Tentang Toleransi Dan Keberagaman Di Lingkungan Tempat Tinggal)
            </div>

            <button type="submit" class="btn btn-primary" style="margin-top: 50px">
                Kirim
            </button>
        </div>
      </form>
</body>
</html>